module.exports = function(config) {

    return {

        get: function(req, cached, error, callback) {
            if(config.fallback == null) {
                // there is not any defined fallback
                callback(null, cached, error, null);
            } else {
                // there is defined fallback, get the message
                config.fallback(req, function(err, message) {
                    callback(err, cached, error, message);
                });
            }
        }

    }

}