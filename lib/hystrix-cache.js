var cache = require('memory-cache');

module.exports = function(config) {

    return {

        get: function(req, callback) {
            if(config.cacheMaxAge == 0) {
                // there is not any defined cache
                callback(null, null);
            } else {
                // there is defined cache
                if(req.hystrixCacheKey == null) {
                    // generate key
                    req.hystrixCacheKey = config.cacheKeyGenerator(req);
                }
                
                value = cache.get(req.hystrixCacheKey);
                
                if(value) {
                    // return cached value
                    callback(null, {
                        isValid: config.cacheMaxAge < 0 ? true : value.timestamp + config.cacheMaxAge > new Date().getTime(),
                        statusCode: value.statusCode, 
                        headers: value.headers, 
                        message: value.message,
                        timestamp: value.timestamp
                    });
                } else {
                    callback(null, null);
                }
            }            
        },

        set: function(req, statusCode, headers, message) {
            if(config.cacheMaxAge != 0) {
                // save new cached value
                cache.put(req.hystrixCacheKey, {
                    statusCode: statusCode, 
                    headers: headers, 
                    message: message,
                    timestamp: new Date().getTime()
                });
            }
        }

    }

}