var hystrixProxy = require('./hystrix-proxy'),
    hystrixCache = require('./hystrix-cache'),
    hystrixFallback = require('./hystrix-fallback'),
    hystrixProcessor = require('./hystrix-processor'),
    circuitBreaker = require('circuitbreaker'),
    querystring = require('querystring'),
    _ = require('lodash');

module.exports = function(config) {

    config = _.defaults(config, { 
        name: undefined,
        hostname: undefined,
        port: undefined,
        path: undefined,
        fallback: undefined, // function(req, callback) { callback(null, message); },
        processor: undefined, // function(message, callback) { callback(null, message); },
        cacheMaxAge: 0,
        cacheKeyGenerator: function(req) {
            return req.params[0] + '?' + querystring.stringify(req.query);
        },        
        breakerTimeout: 10000,
        breakerMaxFailures: 3,
        breakerResetTimeout: 30,
    });

    var proxy = hystrixProxy(config);
    var cache = hystrixCache(config);
    var fallback = hystrixFallback(config);
    var processor = hystrixProcessor(config);
    var breaker = circuitBreaker(proxy.pass, {
        timeout: config.breakerTimeout, 
        maxFailures: config.breakerMaxFailures, 
        resetTimeout: config.breakerResetTimeout 
    });

    return function(req, res, next) {
        if(config === undefined) {
            next(new Error('failed to load service'))
        }

        var fallbackCallback = function(err, cached, error, message) {        
            if(err) {
                // log error
            }

            if(message) {
                // fallback message, return
                response(200, { 'X-Hystrix': 'fallback', 'X-Hystrix-Error': error }, message);
            } else {
                if(cached) {
                    // there is value in cache, return even if not valid
                    response(cached.statusCode, _.defaults({ 'X-Hystrix': 'fallback-cache', 'X-Hystrix-Error': error }, cached.headers), cached.message);
                } else {
                    // error
                    response(500, { 'X-Hystrix': 'fallback', 'X-Hystrix-Error': error });
                }
            }
        }

        var response = function(statusCode, headers, message) {
            res.writeHeader(statusCode, headers);
            if(message != null) {
                if(typeof message.pipe === 'function') {
                    message.pipe(res);
                } else {
                    res.end(message);
                }
            } else {
                res.end(); 
            }
        }

        cache.get(req, function(err, cached) {
            if(err) {
                // log cache error
            }

            if(cached && cached.isValid) {
                // there is value in cache and it is valid, return
                response(cached.statusCode, _.defaults({ 'X-Hystrix': 'cache' }, cached.headers), cached.message);
            } else if(breaker.isOpen()) {
                // circuit is open, fallback
                fallback.get(req, cached, 'circuit is open', fallbackCallback);
            } else {
                var time = new Date().getTime();
                // passing request to third party service   
                breaker(req, res).then(function(data) {
                    // processing request, if defined
                    processor.process(req, data[2], function(err, message) {
                        if(err) {
                            // error, fallback
                            fallback.get(req, cached, err, fallbackCallback);
                        } else {
                            var date = new Date().toISOString();
                            // response is valid, return
                            response(data[0], _.defaults({ 'X-Hystrix': 'proxy', 'X-Hystrix-Date': date, 'X-Hystrix-Response-Time': (new Date().getTime() - time) }, data[1]), message);
                            // and cache it
                            cache.set(req, data[0], _.defaults({ 'X-Hystrix': 'proxy', 'X-Hystrix-Date': date }, data[1]), message);
                        }
                    });
                }).fail(function(err) {
                    fallback.get(req, cached, err, fallbackCallback);
                });
            }
        });
    }

};