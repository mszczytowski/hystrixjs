module.exports = function(config) {

    return {

        process: function(req, message, callback) {
            if(config.processor == null) {
                // there is not any defined processor
                callback(null, message);
            } else {
                config.processor(message, function(err, processedMessage) {
                    callback(err, processedMessage);    
                });
            }
        }

    }

}