var querystring = require('querystring'),
    http = require('http'),
    _ = require('lodash');

module.exports = function(config) {

    return {

        pass: function(req, res, callback) {
            req.pause();

            var options = {
                hostname: config['hostname'],
                port: config['port'],
                path: config['path'] + req.params[0] + '?' + querystring.stringify(req.query),
                headers: _.defaults({ host: config['hostname'] + ':' + config['port'] }, req.headers),
                auth: req.auth,
                method: req.method,
                agent: false
            };

            var reqProxy = http.request(options, function(resProxy) {
                resProxy.pause();

                if(resProxy.statusCode >= 400) {
                    resProxy.resume();
                    callback('invalid status code ' + resProxy.statusCode);
                } else {
                    delete resProxy.headers['Content-Length'];
                    delete resProxy.headers['content-length'];
                    var message = '';
                    resProxy.setEncoding('utf8');
                    resProxy.on('data', function(chunk) {
                        message += chunk;
                    })
                    resProxy.on('end', function() {
                        callback(null, [resProxy.statusCode, resProxy.headers, message]);
                    });
                    resProxy.resume();
                }
            });

            reqProxy.on('error', function(e) {
                callback(e);
            });
            
            req.resume();        
            reqProxy.end();
        }

    }

}